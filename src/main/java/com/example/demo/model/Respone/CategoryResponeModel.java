package com.example.demo.model.Respone;

public class CategoryResponeModel {

    String title;

    public CategoryResponeModel(){}
    public CategoryResponeModel(int id, String title) {

        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return "CategoryResponeModel{" +
                ", title='" + title + '\'' +
                '}';
    }
}
