package com.example.demo.model.Request;

public class CategoryRequestModel {
    String title;

    public CategoryRequestModel(){}
    public CategoryRequestModel(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return "CategoryRequestModel{" +
                "title='" + title + '\'' +
                '}';
    }
}
