package com.example.demo.repository;

import com.example.demo.model.DataTransfer.BookDto;
import com.example.demo.model.DataTransfer.CategoryDto;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BookRepository {

    @Select("SELECT * FROM tb_books")
    @Results({
            @Result(column = "category_id", property = "categoryDto",many = @Many(select = "selectByCateId"))
    })
    List<BookDto> findAll();

    @Insert("INSERT INTO tb_books (title,author,description,thumbnail,category_id) VALUES (#{title},#{author},#{description},#{thumbnail},#{categoryDto.id})")
    boolean insert(BookDto bookDto);

    @Delete("DELETE FROM tb_books WHERE id=#{id}")
    boolean delete(int id);

    @Update("UPDATE tb_books SET title=#{bookDto.title},author=#{bookDto.author},description=#{bookDto.description}, thumbnail=#{bookDto.thumbnail}, category_id=#{bookDto.categoryDto.id} WHERE id=#{id}")
    @Results({
            @Result(column = "category_id", property = "categoryDto")
    })

    boolean update(int id,BookDto bookDto);

    @Select("SELECT * FROM tb_books WHERE id=#{id}")
    @Results({
            @Result(column = "category_id", property = "categoryDto",many = @Many(select = "selectByCateId"))
    })
    BookDto findOne(int id);

    @Select("SELECT * FROM tb_categories WHERE id=#{category_id}")
    CategoryDto selectByCateId(int category_id);

}
